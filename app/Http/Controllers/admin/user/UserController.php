<?php

namespace App\Http\Controllers\admin\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Role;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username'=>'required',
            'password'=>'required|min:3|max:32',
        ], [
             'username.required'=>'Bạn chưa nhập tài khoản',
             'password.required'=>'Bạn chưa nhập mật khẩu',
             'password.min'=>'Mật khẩu phải lớn hơn 3 kí tự',
             'password.max'=>'Mật khẩu phải nhỏ hơn 32 kí tự'   
            ]
        );
        $username = $request->username;
        $password = $request->password;

        if(Auth::attempt(['username' => $username, 'password' => $password])){
            return redirect('/user-index');
        }else{
            return back()->with('thongbao','Đăng nhập thất bại');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request->password);
        User::where('id', $id)->update([
            'username'=>$request->username,
            'email'=>$request->email,
            'phone' =>$request->phone,
            'firstname' =>$request->firstname,
            'lastname' =>$request->lastname,
            'sex' =>$request->sex,
            'password' =>bcrypt($request->password),
            'role' =>$request->role
        ]);

        return back()->with('success','Tạo thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function logOut(){
        Auth::logout();
        return view('admin.user.login');
    }
    public function add_Admin(){
        $role = Role::all();
        return view('admin.user.add_admin',['role' => $role]);
    }
}
