@extends('admin.master')
@section('title','Home')
@section('content')
 <!--App-Content-->
 <div class="app-content  my-3 my-md-5">
    <div class="side-app">
        <div class="page-header">
            <h4 class="page-title">Dashboard OK</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2">
                <div class="card overflow-hidden">
                    <div class="card-body iconfont text-center">
                        <h5>Total Users</h5>
                        <h3 class="counter mb-0 fs-30 mt-1">4698</h3>
                        <p class="text-muted"><span class="text-green"><i
                                    class="fa fa-arrow-up text-green"></i> 23% increase</span> in Last week
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2">
                <div class="card overflow-hidden">
                    <div class="card-body iconfont text-center">
                        <h5>New Clients</h5>
                        <h3 class="counter mb-0 fs-30 mt-1">1425</h3>
                        <p class="text-muted"><span class="text-green"><i
                                    class="fa fa-arrow-up text-green"></i> 12% increase</span> in Last week
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2">
                <div class="card overflow-hidden">
                    <div class="card-body iconfont text-center">
                        <h5>New Cars</h5>
                        <h3 class="counter mb-0 fs-30 mt-1">65874</h3>
                        <p class="text-muted"><span class="text-green"><i
                                    class="fa fa-arrow-up text-green"></i> 12% increase</span> in Last week
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2">
                <div class="card overflow-hidden">
                    <div class="card-body iconfont text-center">
                        <h5>Used Cars</h5>
                        <h3 class="counter mb-0 fs-30 mt-1">2568</h3>
                        <p class="text-muted"><span class="text-danger"><i
                                    class="fa fa-arrow-down text-danger"></i> 5% decrease</span> in Last
                            week</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2">
                <div class="card overflow-hidden">
                    <div class="card-body iconfont text-center">
                        <h5>Orders</h5>
                        <h3 class="counter mb-0 fs-30 mt-1">8965</h3>
                        <p class="text-muted"><span class="text-danger"><i
                                    class="fa fa-arrow-down text-danger"></i> 10% decrease</span> in Last
                            week</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2">
                <div class="card overflow-hidden">
                    <div class="card-body iconfont text-center">
                        <h5>Sales</h5>
                        <h3 class="counter mb-0 fs-30 mt-1">7854</h3>
                        <p class="text-muted"><span class="text-green"><i
                                    class="fa fa-arrow-up text-success"></i> 15% decrease</span> in Last
                            week</p>
                    </div>
                </div>
            </div>
        </div> <!-- ROW-2 OPEN -->
        <div class="row row-deck">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                <div class="card overflow-hidden">
                    <div class="card-header">
                        <h3 class="card-title">Total Sales</h3>
                    </div>
                    <div class="card-body overflow-hidden pb-0">
                        <h2 class="mb-0 font-rubik fs-40">$1,87,595</h2> <small class="text-muted">15%
                            Higher Of Previous Month</small>
                        <div class="row mt-4">
                            <div class="col-sm-12">
                                <div class="mb-0">
                                    <h4 class="mb-2 d-block"> <span class="fs-16">Total Cars</span> <span
                                            class="float-right font-rubik fs-30">1587</span> </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chart-wrapper">
                        <div class="chartjs-size-monitor"
                            style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                            <div class="chartjs-size-monitor-expand"
                                style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                <div
                                    style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
                                </div>
                            </div>
                            <div class="chartjs-size-monitor-shrink"
                                style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                            </div>
                        </div> <canvas id="total-budget"
                            class="chart-dropshadow overflow-hidden chartjs-render-monitor" width="463"
                            height="250" style="display: block; height: 200px; width: 371px;"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                <div class="card overflow-hidden">
                    <div class="card-body text-center overflow-hidden">
                        <div class="mb-4">
                            <h5 class="card-title">Amount of Sales</h5>
                        </div>
                        <h2 class="mb-1 font-rubik fs-40">7,423</h2>
                        <div class="mb-1"> <small class="text-primary mb-0">+15%</small> <small
                                class="text-muted ml-2">From Last Month</small> </div>
                        <div class="chart-wrapper">
                            <div class="chartjs-size-monitor"
                                style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                <div class="chartjs-size-monitor-expand"
                                    style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div
                                        style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
                                    </div>
                                </div>
                                <div class="chartjs-size-monitor-shrink"
                                    style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0">
                                    </div>
                                </div>
                            </div> <canvas id="expense"
                                class="chart-dropshadow2 h-280 overflow-hidden chartjs-render-monitor"
                                style="display: block; height: 280px; width: 323px;" width="403"
                                height="350"></canvas>
                        </div>
                    </div>
                </div>
            </div><!-- COL END -->
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                <div class="card overflow-hidden">
                    <div class="card-header">
                        <h3 class="card-title">Leads By Make Model</h3>
                    </div>
                    <div class="card-body">
                        <div class="">
                            <p class="mb-0">Perspiciatis</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="progress h-3 w-100 mt-1 flex-grow-1 mr-4">
                                    <div class="progress-bar bg-primary w-80"></div>
                                </div>
                                <p class="text-muted mt-1 mb-0">80%</p>
                            </div>
                        </div>
                        <div class="mt-3">
                            <p class="mb-0">Slea</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="progress h-3 w-100 mt-1 flex-grow-1 mr-4">
                                    <div class="progress-bar bg-secondary w-50"></div>
                                </div>
                                <p class="text-muted mt-1 mb-0">50%</p>
                            </div>
                        </div>
                        <div class="mt-3">
                            <p class=" mb-0">Ellesmere</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="progress h-3 w-100 mt-1 flex-grow-1 mr-4">
                                    <div class="progress-bar bg-danger w-65"></div>
                                </div>
                                <p class="text-muted mt-1 mb-0">65%</p>
                            </div>
                        </div>
                        <div class="mt-3">
                            <p class=" mb-0">Voluptates</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="progress h-3 w-100 mt-1 flex-grow-1 mr-4">
                                    <div class="progress-bar bg-success w-30"></div>
                                </div>
                                <p class="text-muted mt-1 mb-0">30%</p>
                            </div>
                        </div>
                        <div class="mt-3">
                            <p class=" mb-0">Crusader</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="progress h-3 w-100 mt-1 flex-grow-1 mr-4">
                                    <div class="progress-bar bg-purple w-40"></div>
                                </div>
                                <p class="text-muted mt-1 mb-0">40%</p>
                            </div>
                        </div>
                        <div class="mt-3">
                            <p class=" mb-0">Interstate</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="progress h-3 w-100 mt-1 flex-grow-1 mr-4">
                                    <div class="progress-bar bg-info w-60"></div>
                                </div>
                                <p class="text-muted mt-1 mb-0">60%</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- ROW-2 CLOSED -->
        <div class="row">
            <div class="col-xl-8 col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Top Inquires</h3>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table card-table table-striped table-vcenter text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Id Number</th>
                                        <th>Car Name</th>
                                        <th>leads</th>
                                        <th>Type</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>#2345</td>
                                        <td>Blanditiis Venue</td>
                                        <td>5</td>
                                        <td>Old</td>
                                        <td class="text-nowrap">05-02-2019</td>
                                        <td class="w-1"><a href="#" class="icon text-danger"><i
                                                    class="fa fa-trash-o"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>#4562</td>
                                        <td>Voluptates XUV300</td>
                                        <td>8</td>
                                        <td>New</td>
                                        <td class="text-nowrap">05-02-2019</td>
                                        <td><a href="#" class="icon text-danger"><i
                                                    class="fa fa-trash-o"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>#8765</td>
                                        <td>Chittenden</td>
                                        <td>7</td>
                                        <td>New</td>
                                        <td class="text-nowrap">05-02-2019</td>
                                        <td><a href="#" class="icon text-danger"><i
                                                    class="fa fa-trash-o"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>#2665</td>
                                        <td>Possimus</td>
                                        <td>10</td>
                                        <td>Old</td>
                                        <td class="text-nowrap">05-02-2019</td>
                                        <td><a href="#" class="icon text-danger"><i
                                                    class="fa fa-trash-o"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>#2846</td>
                                        <td>Jeep Compass</td>
                                        <td>10</td>
                                        <td>New</td>
                                        <td class="text-nowrap">15-08-2019</td>
                                        <td><a href="#" class="icon text-danger"><i
                                                    class="fa fa-trash-o"></i></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-12 col-md-12">
                <div class="card car-comparision">
                    <div class="item7-card-img d-flex"> <img src="assets/images/media/media2.jpg"
                            alt="img" class="cover-image w-50"> <img src="assets/images/media/media4.jpg"
                            alt="img" class="cover-image w-50"> </div>
                    <div class="card-image-difference">Vs</div>
                    <div class="card-body p-4">
                        <div class="row">
                            <div class="col">
                                <h4>Interstate</h4>
                                <h6>$456-$987</h6>
                            </div>
                            <div class="col">
                                <h4>Quaerat</h4>
                                <h6>$785-$841</h6>
                            </div>
                        </div> <a class="btn btn-primary btn-block mt-3" href="car-compare.html">Interstate
                            Vs Quaerat</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Customer Satisfaction</h3>
                    </div>
                    <div class="card-body">
                        <div class="chats-wrap">
                            <div class="chat-details p-2">
                                <h6 class="mb-0"> <span class="font-weight-Automatic">Good</span> <span
                                        class="float-right p-1">78%</span> </h6>
                                <div class="progress progress-sm mt-3">
                                    <div
                                        class="progress-bar progress-bar-striped progress-bar-animated bg-success w-80">
                                    </div>
                                </div>
                            </div>
                            <div class="chat-details p-2">
                                <h6 class="mb-0"> <span class="font-weight-Automatic">Satisfied</span> <span
                                        class="float-right p-1">82%</span> </h6>
                                <div class="progress progress-sm mt-3">
                                    <div
                                        class="progress-bar progress-bar-striped progress-bar-animated bg-primary w-80">
                                    </div>
                                </div>
                            </div>
                            <div class="chat-details p-2">
                                <h6 class="mb-0"> <span class="font-weight-Automatic">Excellent</span> <span
                                        class="float-right p-1">89%</span> </h6>
                                <div class="progress progress-sm mt-3">
                                    <div
                                        class="progress-bar progress-bar-striped progress-bar-animated bg-secondary w-90">
                                    </div>
                                </div>
                            </div>
                            <div class="chat-details p-2">
                                <h6 class="mb-0"> <span class="font-weight-Automatic">Average</span> <span
                                        class="float-right p-1">40%</span> </h6>
                                <div class="progress progress-sm mt-3">
                                    <div
                                        class="progress-bar progress-bar-striped progress-bar-animated bg-warning w-40">
                                    </div>
                                </div>
                            </div>
                            <div class="chat-details p-2">
                                <h6 class="mb-0"> <span class="font-weight-Automatic">Unsatisfied</span>
                                    <span class="float-right p-1">20%</span> </h6>
                                <div class="progress progress-sm mt-3">
                                    <div
                                        class="progress-bar progress-bar-striped progress-bar-animated bg-info w-20">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Marketing Team</div>
                    </div>
                    <div class="card-body">
                        <div class="ibox teams mb-30 bg-boxshadow">
                            <!-- Ibox Content -->
                            <div class="ibox-content teams">
                                <!-- Members -->
                                <div class="avatar-list avatar-list-stacked"> <span
                                        class="avatar brround cover-image cover-image"
                                        data-image-src="assets/images/faces/female/12.jpg"
                                        style="background: url(&quot;assets/images/faces/female/12.jpg&quot;) center center;"></span>
                                    <span class="avatar brround cover-image cover-image"
                                        data-image-src="assets/images/faces/female/21.jpg"
                                        style="background: url(&quot;assets/images/faces/female/21.jpg&quot;) center center;"></span>
                                    <span class="avatar brround cover-image cover-image"
                                        data-image-src="assets/images/faces/female/29.jpg"
                                        style="background: url(&quot;assets/images/faces/female/29.jpg&quot;) center center;"></span>
                                    <span class="avatar brround cover-image cover-image"
                                        data-image-src="assets/images/faces/female/2.jpg"
                                        style="background: url(&quot;assets/images/faces/female/2.jpg&quot;) center center;"></span>
                                    <span class="avatar brround cover-image cover-image"
                                        data-image-src="assets/images/faces/male/34.jpg"
                                        style="background: url(&quot;assets/images/faces/male/34.jpg&quot;) center center;"></span>
                                    <span class="avatar brround cover-image cover-image">+8</span> </div>
                                <!-- Team Board Details -->
                                <div class="teams-board-details mt-3">
                                    <h4 class="font-weight-semibold">About Marketing Team</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam velit
                                        quisquam veniam excepturi. Contrary to popular belief, Lorem Ipsum
                                        is not simply random text classical Latin </p>
                                </div> <!-- Progress Details --> <span
                                    class="font-weight-semibold h5 text-dark">Status of current
                                    properties:</span>
                                <div class="progress-details-teams mt-2 mb-4">
                                    <div class="stat-percent mb-2">58%</div>
                                    <div class="progress progress-sm ">
                                        <div class="progress-bar bg-primary w-50" role="progressbar"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-6">
                                        <div class="teams-rank text-muted mt-2">Used Cars</div> <span
                                            class="h4 mb-0 font-weight-bold">180</span>
                                    </div>
                                    <div class="col-sm-4 col-6">
                                        <div class="teams-rank text-muted mt-2">New Cars</div> <span
                                            class="h4 mb-0 font-weight-bold">1200</span>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <div class="teams-rank text-muted mt-2">BUDGET</div> <span
                                            class="h4 mb-0 font-weight-bold">$36,25,854</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/App-Content-->
@endsection('content')