<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChinhSachesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chinh_saches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name'); //Tên  chính sách
            $table->string('content'); //Nội dung chính sách
            $table->integer('status'); // Trạng thái 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chinh_saches');
    }
}
