<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id'); //Id   
            $table->string('firstname'); // Tên 
            $table->string('lastname');// Họ
            $table->string('img')->nullable();// Ảnh
            $table->string('email')->unique(); // Email
            $table->timestamp('email_verified_at')->nullable(); // Mặc định laravel
            $table->string('phone'); // Số điện thoại
            $table->string('username'); // Tên tài khoản
            $table->string('password'); // Mật khẩu
            $table->integer('sex'); //Giới tính 
            $table->string('country')->nullable(); // Đất Nước
            $table->string('city')->nullable(); // Tỉnh
            $table->string('town')->nullable(); // Quận/Huyện
            $table->string('village')->nullable(); // Phường/Xã
            $table->string('street')->nullable(); //Đường
            $table->string('url_face')->nullable(); // Địa chỉ face
            $table->string('url_google')->nullable(); // Địa chỉ Goolge
            $table->string('url_twitter')->nullable(); // Địa chỉ Twitter
            $table->string('url_print')->nullable(); // ???
            $table->string('aboutme')->nullable();// Thông tin người dùng
            $table->integer('role'); //Mã chức danh
            $table->rememberToken(); // Mã hóa láy lại mật khẩu
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
